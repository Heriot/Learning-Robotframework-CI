*** Settings ***
Library           SeleniumLibrary

*** Keywords ***
Open Browser To main page
    Run Keyword If    '${BROWSER}' == 'HeadlessChrome'    Open Headless Chrome Browser to Page
    ...    ELSE IF    '${BROWSER}' == 'HeadlessFirefox'    Firefox true headless
    ...    ELSE    Open Browser to Page
    Set Selenium Speed    ${DELAY}

Open Headless Chrome Browser to Page
    [Arguments]    ${url}
    ${chrome_options}=    Evaluate    sys.modules['selenium.webdriver'].ChromeOptions()    sys
    Call Method    ${chrome_options}    add_argument    test-type
    Call Method    ${chrome_options}    add_argument    --disable-extensions
    Call Method    ${chrome_options}    add_argument    --headless
    Call Method    ${chrome_options}    add_argument    --disable-gpu
    Call Method    ${chrome_options}    add_argument    --no-sandbox
    Create Webdriver    Chrome    chrome_options=${chrome_options}
    Set Window Size    1920    1080
    Go To    ${url}
