*** Settings ***
Resource          ../Learning-Resource/Keywords/Keywords.robot
Library    OperatingSystem
Library    Collections

*** Test Cases ***
Verify Browser
    [Tags]    active
    Open Headless Chrome Browser to Page    https://www.google.com
    Capture Page Screenshot    ${OUTPUT_DIR}/test.png
    log    สวัสดีครับ
    [Teardown]    Close All Browsers

Get All Env
    ${Test}    Get Environment Variables
    Log List	${Test}
    ${value}    Get Environment Variable    Now_Test
    log    ${value}